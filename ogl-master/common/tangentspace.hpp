#ifndef TANGENTSPACE_HPP
#define TANGENTSPACE_HPP

#include "playground\Components.h"
void computeTangentBasis(
	// inputs
	std::vector<glm::vec3> & vertices,
	std::vector<glm::vec2> & uvs,
	std::vector<glm::vec3> & normals,
	// outputs
	std::vector<glm::vec3> & tangents,
	std::vector<glm::vec3> & bitangents
);
void computeTangentBasis(
	//	Inputs
	std::vector<Vertex>& vertices,
	//	Outputs
	std::vector<TBN>& tbnList
);
void computeTangentBasis(
	//	Inputs
	std::vector<Vertex>& vertices,
	std::vector<aiFace>& faces,
	//	Outputs
	std::vector<TBN>& tbnList
);

#endif