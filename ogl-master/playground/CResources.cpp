#include "CResources.h"
#define STB_IMAGE_IMPLEMENTATION
#include <common\stb_image.h>
std::map<int, Texture*> CResources::loadedTexturesMap;
std::map<string, Material*> CResources::materialMap;
std::map<string, CShader*>	CResources::shaderMap;
std::map<string, CMesh*>	CResources::meshMap;

GameObject* CResources::LoadModelToScene(string path)
{
	CModel model;
	return model.LoadModel(path);
}
Texture* CResources::FindLoadedTextureWithPath(string path)
{
	map<int, Texture*>::iterator it;
	for (it = loadedTexturesMap.begin(); it != loadedTexturesMap.end(); it++)
	{
		if (std::strcmp(it->second->path.data(), path.c_str()) == 0)
		{
			return loadedTexturesMap[it->first];
		}
	}
	return nullptr;
}
unsigned int CResources::TextureFromFile(const char *path, const string &directory, bool gamma)
{
	string filename = string(path);
	filename = directory + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}
