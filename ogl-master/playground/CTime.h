#include <GLFW/glfw3.h>
#pragma once

class CTime
{
public :
	static float deltaTime;
	static float timeScale;
	static float lastTime, curTime;
	static void UpdateDeltaTime();
};

