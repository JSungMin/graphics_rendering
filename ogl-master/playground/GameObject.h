#include "UniObject.h"
#include "Components.h"
#include <glm/gtx/quaternion.hpp>
#include "TypeHelper.h"
#include <vector>
#include <map>
#include <queue>
#include <string>
#include <typeinfo>
#include "Scene.h"
#pragma once

class GameObject
{
private:
	std::map <std::string, void*> componentVectorMap;
	std::vector<void*> componentVectorList;
	//	Helper Function
	template<typename T>
	std::vector<T*>* GetComponentVector()
	{
		std::string typeName = TypeHelper::GetTypeName<T>();
		if (componentVectorMap.find(typeName) == componentVectorMap.end())
		{
			return NULL;
		}
		return static_cast<std::vector<T*>*> (componentVectorMap[typeName]);
	}
public:
	std::string name;
	int tag;
	bool isActive;
	CTransform* transform;

	void SetActive(bool enable)
	{
		isActive = enable;
		if (enable)
			StartComponentsWithChildren();
	}

	//	Called From Scene
	void StartComponentsInThis()
	{
		for (int i = 0; i < componentVectorList.size(); i++)
		{
			std::vector<UniObject*>* compVec = static_cast<std::vector<UniObject*>*>(componentVectorList[i]);
			for (int j = 0; j < compVec->size(); j++)
			{
				if (compVec->at(j)->enable)
					compVec->at(j)->Start();
			}
		}
	}
	void StartComponentsWithChildren()
	{
		std::queue<CTransform*> dfsQ;
		StartComponentsInThis();
		for (int i = 0; i < transform->childCount; i++)
		{
			if (transform->children[i]->gameObject->isActive)
				dfsQ.push(transform->children[i]);
		}
		while (dfsQ.size() != 0)
		{
			CTransform* trans = dfsQ.front();
			trans->gameObject->StartComponentsInThis();
			for (int i = 0; i < trans->childCount; i++)
			{
				if (trans->children[i]->gameObject->isActive)
					dfsQ.push(trans->children[i]);
			}
			dfsQ.pop();
		}
	}
	void UpdateComponentsInThis()
	{
		for (int i = 0; i < componentVectorList.size(); i++)
		{
			std::vector<UniObject*>* compVec = static_cast<std::vector<UniObject*>*>(componentVectorList[i]);
			for (int j = 0; j < compVec->size(); j++)
			{
				if (compVec->at(j)->enable)
					compVec->at(j)->Update();
			}
		}
	}
	void PhysicsUpdateComponentsInThis()
	{
		for (int i = 0; i < componentVectorList.size(); i++)
		{
			std::vector<UniObject*>* compVec = static_cast<std::vector<UniObject*>*>(componentVectorList[i]);
			for (int j = 0; j < compVec->size(); j++)
			{
				if (compVec->at(j)->enable)
					compVec->at(j)->PhysicsUpdate();
			}
		}
	}
	void UpdateComponentsWithChildren()
	{
		std::queue<CTransform*> dfsQ;
		UpdateComponentsInThis();
		for (int i = 0; i < transform->childCount; i++)
		{
			if (transform->children[i]->gameObject->isActive)
				dfsQ.push(transform->children[i]);
		}
		while (dfsQ.size() != 0)
		{
			CTransform* trans = dfsQ.front();
			trans->gameObject->UpdateComponentsInThis();
			for (int i = 0; i < trans->childCount; i++)
			{
				if (trans->children[i]->gameObject->isActive)
					dfsQ.push(trans->children[i]);
			}
			dfsQ.pop();
		}
	}
	void PhysicsUpdateComponentsWithChildren()
	{
		std::queue<CTransform*> dfsQ;
		PhysicsUpdateComponentsInThis();
		for (int i = 0; i < transform->childCount; i++)
		{
			if (transform->children[i]->gameObject->isActive)
				dfsQ.push(transform->children[i]);
		}
		while (dfsQ.size() != 0)
		{
			CTransform* trans = dfsQ.front();
			trans->gameObject->PhysicsUpdateComponentsInThis();
			for (int i = 0; i < trans->childCount; i++)
			{
				if (trans->children[i]->gameObject->isActive)
					dfsQ.push(trans->children[i]);
			}
			dfsQ.pop();
		}
	}
	template<typename T>
	T* AddComponent()
	{
		T* newComp = new T();
		std::vector<T*>* compVec = GetComponentVector<T>();
		if (NULL == compVec)
		{
			std::string typeName = TypeHelper::GetTypeName<T>();
			compVec = new std::vector<T*>();
			compVec->push_back(newComp);
			componentVectorList.push_back(compVec);
			componentVectorMap[typeName] = compVec;
		}
		else
		{
			compVec->push_back(newComp);
		}
		((UniObject*)newComp)->gameObject = this;
		return newComp;
	}
	template<typename T>
	T* GetComponent()
	{ 
		std::vector<T*>* compVec = GetComponentVector<T>();
		if (NULL == compVec)
			return NULL;
		return compVec->at(0);
	}
	template<typename T>
	void RemoveComponent()
	{
		std::vector<T*>* compVec = GetComponentVector<T>();
		if (NULL == compVec)
			return;
		delete compVec->at(compVec->size() - 1);
		compVec->pop_back();
	}
	void RemoveComponent(int index)
	{
		if (index >= componentVectorList.size())
			return;
		std::vector<UniObject*>* compVec = static_cast<std::vector<UniObject*>*>(componentVectorList[index]);
		delete compVec->at(compVec->size() - 1);
		compVec->pop_back();
	}
	void DestroyGameObject()
	{
		delete this;
	}
	//	하이어라키에 생성된 GameObject가 등록된다.
	static GameObject* Instantiate (std::string name)
	{
		GameObject* newObj = new GameObject();
		newObj->name = name;
		newObj->tag = 0;
		newObj->isActive = true;
		newObj->transform = newObj->AddComponent<CTransform>();
		//	부모가 지정안됐다는 것은 하이어라키 Root에 존재한다는 뜻
		Scene::currentScene->PushToHierarchy(newObj);
		return newObj;
	}
	//	하이어라키엔 등록되지 않는다.
	static GameObject* Instantiate(std::string name, CTransform* parent)
	{
		GameObject* newObj = new GameObject();
		newObj->name = name;
		newObj->tag = 0;
		newObj->isActive = true;
		newObj->transform = newObj->AddComponent<CTransform>();
		newObj->transform->SetParent(parent);
		newObj->transform->SetWorldPosition(parent->GetWorldPosition());
		newObj->transform->SetWorldRotation(parent->GetWorldRotationVec3());
		newObj->transform->SetWorldScale(parent->GetWorldScale());
		return newObj;
	}
};