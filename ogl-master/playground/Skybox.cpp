#include "Skybox.h"
#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

float Skybox::skyboxVertices[] = {
	// positions          
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f
};
GLuint Skybox::cubmapTex;
GLuint Skybox::skyboxShaderID;
GLuint Skybox::skyboxVAO;
GLuint Skybox::skyboxVBO;

void Skybox::Initialize(std::vector<const char*> faces, CShader shader)
{
	// skybox VAO
	skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	cubmapTex = loadCubeMapDDS(faces);
	skyboxShaderID = shader.id;

	glUseProgram(skyboxShaderID);
	glUniform1i(cubmapTex, 0);
}

void Skybox::Render()
{
	glDepthFunc(GL_LEQUAL);
	glUseProgram(skyboxShaderID);
	CCamera* camComp = CCamera::mainCam;
	glm::mat4 view = glm::mat4(glm::mat3(camComp->GetViewMatrix()));
	glm::mat4 proj = camComp->GetProjectionMatrix();
	GLuint ViewID = glGetUniformLocation(skyboxShaderID, "view");
	GLuint ProjID = glGetUniformLocation(skyboxShaderID, "projection");
	glUniformMatrix4fv(ViewID, 1, GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(ProjID, 1, GL_FALSE, &proj[0][0]);

	glBindVertexArray(skyboxVAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubmapTex);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthFunc(GL_LESS);
}

GLuint Skybox::loadCubeMapDDS(std::vector<const char*> faces)
{
	GLuint textureID;

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	unsigned char header[124];
	unsigned int height[6], width[6];
	unsigned int linearSize[6], mipMapCount[6], fourCC[6];
	FILE *fp;
	for (int i = 0; i < faces.size(); i++)
	{
		/* try to open the file */
		fp = fopen(faces[i], "rb");
		if (fp == NULL) {
			printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", faces[i]); getchar();
			return 0;
		}
		/* verify the type of file */
		char filecode[4];
		fread(filecode, 1, 4, fp);
		if (strncmp(filecode, "DDS ", 4) != 0) {
			fclose(fp);
			return 0;
		}
		/* get the surface desc */
		fread(&header, 124, 1, fp);

		height[i] = *(unsigned int*)&(header[8]);
		width[i] = *(unsigned int*)&(header[12]);
		linearSize[i] = *(unsigned int*)&(header[16]);
		mipMapCount[i] = *(unsigned int*)&(header[24]);
		fourCC[i] = *(unsigned int*)&(header[80]);

		unsigned char * buffer;
		unsigned int bufsize;
		/* how big is it going to be including all mipmaps? */
		bufsize = mipMapCount[i] > 1 ? linearSize[i] * 2 : linearSize[i];
		buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
		fread(buffer, 1, bufsize, fp);
		/* close the file pointer */
		fclose(fp);
		unsigned int components = (fourCC[i] == FOURCC_DXT1) ? 3 : 4;
		unsigned int format;
		switch (fourCC[i])
		{
		case FOURCC_DXT1:
			format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			break;
		case FOURCC_DXT3:
			format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			break;
		case FOURCC_DXT5:
			format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			break;
		default:
			free(buffer);
			return 0;
		}

		unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
		unsigned int offset = 0;
		/* load the mipmaps */
		for (unsigned int level = 0; level < mipMapCount[i] && (width[i] || height[i]); ++level)
		{
			// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
			if (width[i] == 0) width[i] = 1;
			if (height[i] == 0) height[i] = 1;
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);

			unsigned int size = ((width[i] + 3) / 4)*((height[i] + 3) / 4)*blockSize;
			glCompressedTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, level, format, width[i], height[i],
				0, size, buffer + offset);

			offset += size;
			width[i] /= 2;
			height[i] /= 2;
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		free(buffer);
	}
	return textureID;
}
