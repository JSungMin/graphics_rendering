#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexCoords;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec3 aTangent;
layout(location = 4) in vec3 aBitangent;

#define NR_POINT_LIGHTS 4

out VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
	mat3 TBN;
    vec3 TangentLightPos[NR_POINT_LIGHTS];
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform vec3 lightPos[NR_POINT_LIGHTS];
uniform vec3 viewPos;

void main(){
	// 정점의 출력 좌표, clip space에선 : MVP * position 
	vs_out.FragPos = vec3(model * vec4(aPos, 1.0));
	vs_out.TexCoords = aTexCoords;

	mat3 normalMatrix = transpose(inverse(mat3(model)));
    vec3 T = normalize(normalMatrix * aTangent);
    vec3 N = normalize(normalMatrix * aNormal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = normalize(normalMatrix * aBitangent);

	if (dot(cross(N, T), B) < 0.0)
                T = T * -1.0;

	mat3 TBN = mat3(T,B,N);
	vs_out.TBN = TBN;
	for (int i = 0; i < NR_POINT_LIGHTS; i++)
	{
		vs_out.TangentLightPos[i] = TBN * lightPos[i];
	}
    vs_out.TangentViewPos  = TBN * viewPos;
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;

	gl_Position = proj * view * model * vec4(aPos, 1.0);
}

