#include "GameObject.h"
#include "Components.h"
#pragma once

class RocketLauncher : public UniObject
{
public:
	GameObject *rocketRoot;
	GameObject *rocketBody;
	GameObject *rocketEngine;
	CTransform *paraTrans;
	CRigid* rootRigid;
	CRigid* bodyRigid;
	CRigid* engineRigid;
	//	0 : CHARGING
	//	1 : FLING
	//	2 : BODY_SEPARATION
	//	3 : Inertia_FLING
	//	4 : FREE_FALL
	//	5 : PARACHUTE
	//	6 : SLOW_FALLING
	//	7 : LANDING
	int rocketState = 0;
	float rocketFuel = 15;
	float initSpeed = 10;

	int cameraState = 0;

	void Start() override
	{
		initSpeed = 35.0f;
		//initSpeed = 50.0f;
	}
	void Update() override
	{
		if (CInput::GetKey(GLFW_KEY_SPACE) && rocketState == 0)
		{
			rocketState = 1;
		}
		if (rocketState == 1)
		{
			if (rocketFuel > 0)
			{
				rootRigid->AddForce(glm::vec3(0, 1, 0) * initSpeed);
				rocketFuel -= CTime::deltaTime * 5;
			}
			else
			{
				rocketState = 2;
				rocketFuel = 0;
			}
		}
		else if (rocketState == 2)
		{
			rocketEngine->transform->SetParent(NULL);
			engineRigid = rocketEngine->AddComponent<CRigid>();
			engineRigid->AddForce(glm::vec3(3000,0,0));
			rocketState = 3;
		}
		else if (rocketState == 3)
		{
			//	Rocket ��ü�� �������� ������
			if (rootRigid->velocity.y <= 0)
			{
				//	Set FreeFall
				rocketState = 4;
			}
		}
		else if (rocketState == 4)
		{
			//	�������� �� ������ȯ
			if (rocketRoot->transform->GetWorldRotationVec3().z < 270)
			{
				rocketRoot->transform->Rotate(glm::vec3(0,0,360) * CTime::deltaTime);
			}
			else
			{
				rocketRoot->transform->SetWorldRotation(glm::vec3(0,0,270));
				//	���ϻ� ��ġ��
				if (CInput::GetKey(GLFW_KEY_SPACE))
				{
					rocketBody->transform->SetParent(NULL);
					bodyRigid = rocketBody->AddComponent<CRigid>();
					bodyRigid->AddForce(glm::vec3(-1000, 0, -1500));
					rocketState = 5;
				}
			}
		}
		else if (rocketState == 5)
		{
			if (rocketRoot->transform->GetWorldRotationVec3().z < 450)
			{
				rocketRoot->transform->Rotate(glm::vec3(0, 0, 540) * CTime::deltaTime);
			}
			else
			{
				//	ȸ�� �Ϸ�
				if (paraTrans->GetLocalScale().x < 0.1)
				{
					paraTrans->Scale(glm::vec3(0.1, 0.1, 0.1) * CTime::deltaTime);
				}
				else
				{
					//	���ϻ� ��ġ�� �Ϸ�
					paraTrans->SetLocalScale(glm::vec3(0.1, 0.1, 0.1));
					rootRigid->velocity = glm::vec3(0,-rootRigid->velocity.y * 0.1f,0);
					rootRigid->useGravity = false;
					rocketState = 6;
				}
			}
		}
		else if (rocketState == 6)
		{
			//	FALLING_SLOWLY
			if (rootRigid->velocity.y > -5)
				rootRigid->AddForce(-rootRigid->gravity * 0.8f * rootRigid->mass);
			else
				rootRigid->velocity = glm::vec3(0,-5,0);
		}
		else if (rocketState == 7)
		{
			if (paraTrans->GetWorldRotationVec3().y < 90)
			{
				paraTrans->Scale(glm::vec3(-0.01, -0.02, -0.03) * CTime::deltaTime);
				paraTrans->Rotate(glm::vec3(0, 30, 0) * CTime::deltaTime);
			}
			else if (paraTrans->GetLocalScale().z > 0)
			{
				paraTrans->Scale(glm::vec3(-0.1, -0.2, -0.2) * CTime::deltaTime);
			}
			else
			{
				paraTrans->SetWorldScale(glm::vec3(0, 0, 0));
			}
		}

		if (rocketState >= 3)
		{
			if (rocketEngine->transform->GetWorldPosition().y <= -5 && engineRigid->velocity.y <= 0)
			{
				engineRigid->velocity = glm::vec3(0, 0, 0);
				engineRigid->enable = false;
				glm::vec3 adjPos = rocketEngine->transform->GetWorldPosition();
				adjPos.y = -5;
				rocketEngine->transform->SetWorldPosition(adjPos);
			}
			else
				rocketEngine->transform->Rotate(glm::vec3(0, 0, 240) * CTime::deltaTime);
		}
		if (rocketState >= 5 && NULL != bodyRigid)
		{
			if (rocketBody->transform->GetWorldPosition().y <= -5 && bodyRigid->velocity.y <= 0)
			{
				bodyRigid->velocity = glm::vec3(0, 0, 0);
				bodyRigid->enable = false;
				glm::vec3 adjPos = rocketBody->transform->GetWorldPosition();
				adjPos.y = -5;
				rocketBody->transform->SetWorldPosition(adjPos);
			}
			else
				rocketBody->transform->Rotate(glm::vec3(0, 0, 240) * CTime::deltaTime);
		}
		if (rocketState == 0)
		{
			rootRigid->velocity = glm::vec3(0,0,0);
			rocketRoot->transform->SetWorldPosition(glm::vec3(0,0,0));
		}
		else if (rocketRoot->transform->GetWorldPosition().y <= -5.1 && rootRigid->velocity.y < 0)
		{
			//	LANDING
			rocketRoot->transform->SetWorldPosition(glm::vec3(0, -5.1, 0));
			rocketState = 7;
		}
		//	Camera Teleport
		if (CInput::GetKey(GLFW_KEY_1))
			cameraState = 0;
		else if (CInput::GetKey(GLFW_KEY_2))
			cameraState = 1;
		else if (CInput::GetKey(GLFW_KEY_3))
			cameraState = 2;
		else if (CInput::GetKey(GLFW_KEY_4))
			cameraState = 3;

		if (cameraState == 0)
		{
			CTransform* camTrans = CCamera::mainCam->gameObject->transform;
			glm::vec3 adjPos = camTrans->GetWorldPosition();
			adjPos.y = rocketRoot->transform->GetWorldPosition().y;
			camTrans->SetWorldPosition(adjPos);
		}
		else if (cameraState == 1)
		{
			CTransform* camTrans = CCamera::mainCam->gameObject->transform;
			glm::vec3 adjPos = camTrans->GetWorldPosition();
			adjPos.y = rocketEngine->transform->GetWorldPosition().y;
			camTrans->SetWorldPosition(adjPos);
		}
		else if (cameraState == 2)
		{
			CTransform* camTrans = CCamera::mainCam->gameObject->transform;
			glm::vec3 adjPos = camTrans->GetWorldPosition();
			adjPos.y = rocketBody->transform->GetWorldPosition().y;
			camTrans->SetWorldPosition(adjPos);
		}
	}
};
