#include "Components.h"
#pragma once

class CPrimitiveMesh
{
private:

public:
	static CMesh GetCube(glm::vec3 extent);
	static CMesh GetSphere(glm::vec3 extent);
};
