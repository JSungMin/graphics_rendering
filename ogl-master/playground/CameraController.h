#include "GameObject.h"
#include "Components.h"
#pragma once

class CameraController : public UniObject
{
private:
	unsigned short kForward, kBack;
	unsigned short kRight, kLeft;
	unsigned short kUp, kDown;

	glm::vec3 deltaPos;
	float mx, my;
public:
	CTransform *camTrans;
	GLFWwindow* window;

	float moveSpeed = 10;
	float rotSpeed = 30;

	// UniObject을(를) 통해 상속됨
	void Start() override
	{
		camTrans = gameObject->transform;
		window = Scene::window;
	}
	void Update() override
	{
		kForward = glfwGetKey(window, GLFW_KEY_UP);
		kBack = glfwGetKey(window, GLFW_KEY_DOWN);
		kRight = glfwGetKey(window, GLFW_KEY_RIGHT);
		kLeft = glfwGetKey(window, GLFW_KEY_LEFT);
		kUp = glfwGetKey(window, GLFW_KEY_Q);
		kDown = glfwGetKey(window, GLFW_KEY_R);
		deltaPos = glm::vec3();
		mx = my = 0;

		if (kForward == GLFW_PRESS)
		{
			//deltaPos += camTrans->forward * moveSpeed * CTime::deltaTime;
			deltaPos += glm::vec3(0,0,1) * moveSpeed * CTime::deltaTime;
		}
		else if (kBack == GLFW_PRESS)
		{
			//deltaPos -= camTrans->forward * moveSpeed * CTime::deltaTime;
			deltaPos -= glm::vec3(0, 0, 1) * moveSpeed * CTime::deltaTime;
		}
		if (kRight == GLFW_PRESS)
		{
			deltaPos += glm::vec3(0, 1, 0) * moveSpeed * CTime::deltaTime;
			//deltaPos += camTrans->right * moveSpeed * CTime::deltaTime;
		}
		else if (kLeft == GLFW_PRESS)
		{
			deltaPos -= glm::vec3(0, 1, 0) * moveSpeed * CTime::deltaTime;
			//deltaPos -= camTrans->right * moveSpeed * CTime::deltaTime;
		}
		if (kUp == GLFW_PRESS)
		{
			deltaPos += glm::vec3(0, 1, 0) * moveSpeed * CTime::deltaTime;
		}
		else if (kDown == GLFW_PRESS)
		{
			deltaPos -= glm::vec3(0, 1, 0) * moveSpeed * CTime::deltaTime;
		}
		camTrans->Translate(deltaPos);
		//camTrans->Rotate(deltaPos);
		// Get mouse position
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);

		// Reset mouse position for next frame
		glfwSetCursorPos(window, 1024 / 2, 768 / 2);

		// Compute new orientation
		//mx = glm::sign(camTrans->up.y) * rotSpeed * float(xpos - 1024 / 2) * CTime::deltaTime;
		//my = glm::sign(camTrans->right.x * camTrans->forward.z) * rotSpeed * float(ypos - 768 / 2) * CTime::deltaTime;
		//camTrans->Rotate(glm::vec3(my,mx,0));
	}

};
