#include "Scene.h"
#include "GameObject.h"

Scene* Scene::currentScene;
GLFWwindow* Scene::window;

void Scene::PushToHierarchy(GameObject * obj)
{
	hierarchy.push_back(obj);
}
void Scene::PopFromHierarchy(GameObject * obj)
{
	for (int i = 0; i < hierarchy.size(); i++)
	{
		if (hierarchy[i] == obj)
		{
			hierarchy.erase(hierarchy.begin() + i);
			return;
		}
	}
}
void Scene::StartScene()
{
	for (int i = 0; i < hierarchy.size(); i++)
	{
		if (hierarchy[i]->isActive)
			hierarchy[i]->StartComponentsWithChildren();
	}
}
void Scene::UpdateScene()
{
	for (int i = 0; i < hierarchy.size(); i++)
	{
		if (hierarchy[i]->isActive)
			hierarchy[i]->UpdateComponentsWithChildren();
	}
}
void Scene::UpdatePhysics()
{
	for (int i = 0; i < hierarchy.size(); i++)
	{
		if (hierarchy[i]->isActive)
			hierarchy[i]->PhysicsUpdateComponentsWithChildren();
	}
}
