#include "Components.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#pragma once

using namespace glm;

void CTransform::SetParent(CTransform* parent)
{
	//	현 부모로부터 독립
	if (this->parent != NULL)
	{
		bool findChk = false;
		int i = 0;
		for (i; i < this->parent->childCount; i++)
		{
			if (this->parent->children[i] == this)
			{
				findChk = true;
				break;
			}
		}
		if (findChk)
		{
			CTransform* prevParent = this->parent;
			this->parent = NULL;
			SetWorldScale(prevParent->mWorldScale * mLocalScale);
			SetWorldRotation(prevParent->mWorldRotation + mLocalRotation);
			glm::quat radQuat = glm::quat();
			radQuat = glm::rotate(radQuat,
				glm::radians(mWorldRotation.x),
				glm::vec3(1,0,0)
			);
			radQuat = glm::rotate(radQuat,
				glm::radians(mWorldRotation.y),
				glm::vec3(0, 1, 0)
			);
			radQuat = glm::rotate(radQuat,
				glm::radians(mWorldRotation.z),
				glm::vec3(0, 0, 1)
			);
			glm::vec3 rotatedVec = prevParent->mWorldPosition + radQuat * mLocalPosition;
			SetWorldPosition(rotatedVec);
			prevParent->children.erase(prevParent->children.begin() + i);
			prevParent->childCount--;
		}
	}
	//	입양하려는 부모가 없을 경우
	if (parent == NULL)
	{
		Scene::currentScene->PushToHierarchy(gameObject);
		this->parent = NULL;
		return;
	}
	else	//	입양하려는 부모가 있는 경우
	{
		Scene::currentScene->PopFromHierarchy(gameObject);
		this->parent = parent;
		parent->children.push_back(this);
		parent->childCount++;
	}
}
void CTransform::SetWorldPosition(glm::vec3 pos)
{
	mWorldPosition = pos;
	if (NULL != parent)
		mLocalPosition = mWorldPosition - parent->mWorldPosition;
	else
		mLocalPosition = mWorldPosition;
}
void CTransform::SetLocalPosition(glm::vec3 pos)
{
	mLocalPosition = pos;
	if (NULL != parent)
		mWorldPosition = parent->mWorldPosition + mLocalPosition;
	else
		mWorldPosition = mLocalPosition;
}
void CTransform::SetWorldScale(glm::vec3 scale)
{
	mWorldScale = scale;
	if (NULL != parent)
	{
		mLocalScale.x = mWorldScale.x / parent->mWorldScale.x;
		mLocalScale.y = mWorldScale.y / parent->mWorldScale.y;
		mLocalScale.z = mWorldScale.z / parent->mWorldScale.z;
	}
	else
		mLocalScale = mWorldScale;
}
void CTransform::SetLocalScale(glm::vec3 scale)
{
	mLocalScale = scale;
	if (NULL != parent)
	{
		mWorldScale.x = parent->mWorldScale.x * mLocalScale.x;
		mWorldScale.y = parent->mWorldScale.y * mLocalScale.y;
		mWorldScale.z = parent->mWorldScale.z * mLocalScale.z;
	}
	else
		mWorldScale = mLocalScale;
}
void CTransform::SetWorldRotation(glm::vec3 rotation)
{
	mWorldRotation = rotation;
	if (NULL != parent)
		mLocalRotation = mWorldRotation - parent->mWorldRotation;
	else
		mLocalRotation = mWorldRotation;
}
void CTransform::SetLocalRotation(glm::vec3 rotation)
{
	mLocalRotation = rotation;
	if (NULL != parent)
		mWorldRotation = parent->mWorldRotation + mLocalRotation;
	else
		mWorldRotation = mLocalRotation;
}
glm::vec3 CTransform::GetWorldPosition()
{
	return mWorldPosition;
}
glm::vec3 CTransform::GetLocalPosition()
{
	return mLocalPosition;
}
glm::quat CTransform::GetWorldRotation()
{
	return glm::quat(mWorldRotation);
}
glm::quat CTransform::GetLocalRotation()
{
	return glm::quat(mLocalRotation);
}
glm::vec3 CTransform::GetWorldRotationVec3()
{
	return mWorldRotation;
}
glm::vec3 CTransform::GetLocalRotationVec3()
{
	return mLocalRotation;
}
glm::vec3 CTransform::GetWorldScale()
{
	return mWorldScale;
}

glm::vec3 CTransform::GetLocalScale()
{
	return mLocalScale;
}

void CTransform::Translate(glm::vec3 delta)
{
	SetLocalPosition(mLocalPosition + delta);
}

void CTransform::Rotate(glm::vec3 delta)
{
	SetWorldRotation(mWorldRotation + delta);
}

void CTransform::Scale(glm::vec3 delta)
{
	SetLocalScale(mLocalScale + delta);
}

void CTransform::UpdateWorldMatrix()
{
	if (NULL != parent)
	{
		glm::quat mQuat = GetLocalRotation();
		glm::mat4 parentMat =
			glm::translate(glm::mat4(), parent->mWorldPosition) *
			glm::rotate(glm::mat4(), glm::radians(parent->mWorldRotation.x), glm::vec3(1, 0, 0)) *
			glm::rotate(glm::mat4(), glm::radians(parent->mWorldRotation.y), glm::vec3(0, 1, 0)) *
			glm::rotate(glm::mat4(), glm::radians(parent->mWorldRotation.z), glm::vec3(0, 0, 1));
		mWorldMatrix = parentMat * glm::translate(glm::mat4(), mLocalPosition) *
			glm::rotate(glm::mat4(), glm::radians(mLocalRotation.x), glm::vec3(1, 0, 0)) *
			glm::rotate(glm::mat4(), glm::radians(mLocalRotation.y), glm::vec3(0, 1, 0)) *
			glm::rotate(glm::mat4(), glm::radians(mLocalRotation.z), glm::vec3(0, 0, 1)) *
			glm::scale(mWorldScale);
		SetLocalPosition(mLocalPosition);
		SetLocalRotation(mLocalRotation);
		SetLocalScale(mLocalScale);
	}
	else
	{
		glm::quat mQuat = GetWorldRotation();
		
		mWorldMatrix = glm::translate(glm::mat4(), mWorldPosition) *
			glm::rotate(glm::mat4(), glm::radians(mWorldRotation.x), glm::vec3(1, 0, 0)) *
			glm::rotate(glm::mat4(), glm::radians(mWorldRotation.y), glm::vec3(0, 1, 0)) *
			glm::rotate(glm::mat4(), glm::radians(mWorldRotation.z), glm::vec3(0, 0, 1)) *
			glm::scale(mWorldScale);
	}
	up = glm::normalize(glm::vec3(mWorldMatrix[0][1], mWorldMatrix[1][1], mWorldMatrix[2][1]));
	forward = glm::normalize(glm::vec3(mWorldMatrix[0][2], mWorldMatrix[1][2], mWorldMatrix[2][2]));
	right = glm::normalize(glm::cross(forward, up));
}
glm::mat4 CTransform::GetWorldMatrix()
{
	return mWorldMatrix;
}

void CTransform::Start()
{

}

void CTransform::Update()
{
	//	Update World Model Matrix
	UpdateWorldMatrix();
}
