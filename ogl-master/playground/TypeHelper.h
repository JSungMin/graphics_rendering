#include <typeinfo>
#include <iostream>
#include <string>
#pragma once

class TypeHelper
{
public:
	template<typename T>
	static std::string GetTypeName()
	{
		return typeid(T).name();
	}
};
