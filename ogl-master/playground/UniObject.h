#include <stdio.h>
#include <stdlib.h>
#include <string>
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
// Include GLM
#include <glm/glm.hpp>

#pragma once

//	상호참조 해결을 위한 GameObject 선언
class GameObject;
class UniComponent
{
public:
#pragma region PUBLIC VARIABLE BLOCK
	std::string name;
	bool enable;
	GameObject* gameObject;
#pragma endregion
#pragma region CONSTRUCTOR
	UniComponent() : name("NO_NAME"), enable(true) {	}
	UniComponent(std::string n) : name(n), enable(true) {	}
#pragma endregion
#pragma region PURE VIRTUAL FUNCTION BLOCK
	virtual void Start() = 0;
	virtual void Update() = 0;
	virtual void PhysicsUpdate()
	{

	}
#pragma endregion

};