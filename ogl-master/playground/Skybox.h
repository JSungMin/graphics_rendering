#include "Components.h"
#pragma once

class Skybox
{
public:
	static float skyboxVertices[108];
	static GLuint cubmapTex;
	static GLuint skyboxShaderID;
	static GLuint skyboxVAO, skyboxVBO;
	static void Initialize(std::vector<const char*> faces, CShader shader);
	static void Render();

	static GLuint loadCubeMapDDS(std::vector<const char*> faces);
};