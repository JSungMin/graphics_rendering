#include "Components.h"
#include "GameObject.h"
void CRigid::Start()
{

}
void CRigid::Update()
{
	
}
void CRigid::PhysicsUpdate()
{
	if (useGravity)
		GravityUpdate();
	else
		NoneGravityUpdate();
}
void CRigid::NoneGravityUpdate()
{
	gameObject->transform->Translate(velocity * CTime::deltaTime);
}
void CRigid::GravityUpdate()
{
	gameObject->transform->Translate(velocity * CTime::deltaTime);
	velocity -= gravity * CTime::deltaTime;
}
void CRigid::AddForce(glm::vec3 force)
{
	velocity += (force / mass) * CTime::deltaTime;
}
