#include "Components.h"
#include <map>
#include <string>
#pragma once

using namespace std;

class CResources
{
public:
	static std::map<int, Texture*> loadedTexturesMap;
	static std::map<string, Material*>	materialMap;
	static std::map<string, CShader*>	shaderMap;
	static std::map<string, CMesh*>		meshMap;
	static GameObject* LoadModelToScene(string path);
	static Texture* FindLoadedTextureWithPath(string path);
	static unsigned int TextureFromFile(const char *path, const string &directory, bool gamma = false);
};
