#include "Components.h"
#include "common/tangentspace.hpp"
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

using namespace std;
using namespace glm;

CMesh::CMesh(
	std::vector<Vertex> vertices,
	std::vector<TBN>	tbnList,
	std::vector<unsigned short> indices,
	std::vector<Texture> textures)
{
	this->vertices = vertices;
	this->tbnList = tbnList;
	this->indices = indices;
	this->textures = textures;

	SetUpMesh();
}
void CMesh::Draw(CShader shader)
{
	unsigned int diffuseN = 1;
	unsigned int normalN = 1;
	unsigned int specularN = 1;
	for (int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // 바인딩하기 전에 적절한 텍스처 유닛 활성화
		string name;
		string number;
		switch (textures[i].textureType)
		{
		case Texture::TextureType::Diffuse:
			name = "texture_diffuse";
			number = std::to_string(diffuseN++);
			break;
		case Texture::TextureType::Normal:
			name = "texture_normal";
			number = std::to_string(normalN++);
			break;
		case Texture::TextureType::Specular:
			name = "texture_specular";
			number = std::to_string(specularN++);
			break;
		}
		glUniform1i(glGetUniformLocation(shader.id, (name + number).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}

	// mesh 그리기
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
}

void CMesh::SetUpMesh()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &VBO_TBN);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	// vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	// vertex texture coords
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texcoord));
	
	//	When Use Just One VBO -> We might have Memory Crash Risk
	//	So Spilt it struct to VBO and VBO_TBN
	glBindBuffer(GL_ARRAY_BUFFER, VBO_TBN);
	glBufferData(GL_ARRAY_BUFFER, tbnList.size() * sizeof(TBN), &tbnList[0], GL_STATIC_DRAW);
	// vertex normals
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(TBN), (void*)offsetof(TBN, normal));

	// vertex tangents
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(TBN), (void*)offsetof(TBN, tangent));
	
	// vertex tangents
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(TBN), (void*)offsetof(TBN, biTangent));

	glBindVertexArray(0);
}