#include "CTime.h"

float CTime::deltaTime;
float CTime::timeScale = 1;
float CTime::lastTime;
float CTime::curTime;

void CTime::UpdateDeltaTime()
{
	curTime = glfwGetTime();
	deltaTime = (curTime - lastTime) * timeScale;
	lastTime = curTime;
}