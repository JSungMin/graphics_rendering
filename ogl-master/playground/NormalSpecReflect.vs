﻿#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec2 aTexCoords;
layout(location = 2) in vec3 aNormal;
layout(location = 3) in vec3 aTangent;

out VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
	vec3 Normal;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform vec3 lightPos0;
uniform vec3 viewPos;

void main(){
	// ������ ��� ��ǥ, clip space���� : MVP * position 
	vs_out.FragPos = vec3(model * vec4(aPos, 1.0));
	vs_out.TexCoords = aTexCoords;

	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 T = normalize(normalMatrix * aTangent);
	vec3 N = normalize(normalMatrix * aNormal);
	T = normalize(T - dot(T,N) * N);
	vec3 B = cross(N, T);

	mat3 TBN = transpose(mat3(T,B,N));
	vs_out.TangentLightPos = TBN * lightPos0;
    vs_out.TangentViewPos  = TBN * viewPos;
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;
	vs_out.Normal = N;
	
	gl_Position = proj * view * model * vec4(aPos, 1.0);
}

