#include <common\tangentspace.hpp>
#include <common\vboindexer.hpp>
#include "core.h"

using namespace std;
using namespace glm;

GameObject* CModel::LoadModel(string path)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		cout << "ERROR::ASSIMP::" << importer.GetErrorString() << endl;
		return nullptr;
	}
	directory = path.substr(0, path.find_last_of('/'));
	string name = path.substr(path.find_last_of('/', path.length() - 1));
	GameObject* root = GameObject::Instantiate("Model_Root");
	processNode(scene->mRootNode, root, scene);
	return root;
}

void CModel::processNode(aiNode* node,GameObject* targetParent, const aiScene* scene)
{
	GameObject* parent;
	// 노드의 모든 mesh들을 처리(만약 있다면)
	if (node->mNumMeshes > 0)
	{
		GameObject* newChild = GameObject::Instantiate(node->mName.C_Str(), targetParent->transform);
		for (unsigned int i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
			string meshName = mesh->mName.C_Str();
			if ("" == meshName)
				meshName = "mesh" + node->mMeshes[i];
			CResources::meshMap[mesh->mName.C_Str()] = processMesh(mesh, scene);
			GameObject* meshObj = GameObject::Instantiate(mesh->mName.C_Str(), newChild->transform);
			CRenderer* renderer = meshObj->AddComponent<CRenderer>();
			renderer->mesh = CResources::meshMap[mesh->mName.C_Str()];
			renderer->material = CResources::materialMap["DefaultMat"];
		}
		parent = newChild;
	}
	else
	{
		parent = targetParent;
	}
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i],parent,scene);
	}
	/*for (unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(mesh, scene)); 
	}*/
	// 그런 다음 각 자식들에게도 동일하게 적용
	/*for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		NodeWrapper* childNode;
		childNode->node = node->mChildren[i];
		childNode->gameObject = GameObject::Instantiate(childNode->node->mName.C_Str());
		childNode->gameObject->transform->SetParent(gNode->transform);
		processNode(childNode, scene);
	}*/
}
CMesh* CModel::processMesh(aiMesh *mesh, const aiScene *scene)
{
	vector<Vertex>	in_vertices;
	vector<TBN>		in_tbnList;
	vector<unsigned short> in_indices;
	vector<Texture> textures;
	vector<aiFace>	faces;
	
	vector<Vertex> out_vertices;
	vector<TBN>	out_tbnList;
	vector<unsigned short>	out_indices;

	bool hasNormal = true;
	bool hasTangent = true;
	
	if (mesh->HasFaces())
	{
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			faces.push_back(mesh->mFaces[i]);
		}
	}
	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		TBN tbn;
		// vertex 위치, 법선, 텍스처 좌표를 처리
		glm::vec2 tmpVec2;
		glm::vec3 tmpVec3;
		tmpVec3.x = mesh->mVertices[i].x;
		tmpVec3.y = mesh->mVertices[i].y;
		tmpVec3.z = mesh->mVertices[i].z;
		vertex.position = tmpVec3;
		if (mesh->HasNormals())
		{
			tmpVec3.x = mesh->mNormals[i].x;
			tmpVec3.y = mesh->mNormals[i].y;
			tmpVec3.z = mesh->mNormals[i].z;
			tbn.normal = tmpVec3;
		}
		else
		{
			hasNormal = false;
		}
		if (mesh->HasTangentsAndBitangents())
		{
			tmpVec3.x = mesh->mTangents[i].x;
			tmpVec3.y = mesh->mTangents[i].y;
			tmpVec3.z = mesh->mTangents[i].z;
			tbn.tangent = tmpVec3;
			tmpVec3.x = mesh->mBitangents[i].x;
			tmpVec3.y = mesh->mBitangents[i].y;
			tmpVec3.z = mesh->mBitangents[i].z;
			tbn.biTangent = tmpVec3;
		}
		else
		{
			hasTangent = false;
		}
		if (mesh->HasTextureCoords(0))
		{
			tmpVec2.x = mesh->mTextureCoords[0][i].x;
			tmpVec2.y = mesh->mTextureCoords[0][i].y;
			vertex.texcoord = tmpVec2;
		}
		else
			vertex.texcoord = glm::vec2(0.0f, 0.0f);
		in_vertices.push_back(vertex);
		in_tbnList.push_back(tbn);
	}
	// indices 처리
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			in_indices.push_back(face.mIndices[j]);
	}
	if (!hasNormal)
	{
		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			if (face.mNumIndices >= 3)
			{
				glm::vec3 p1, p2, p3;
				p1.x = mesh->mVertices[face.mIndices[0]].x;
				p1.y = mesh->mVertices[face.mIndices[0]].y;
				p1.z = mesh->mVertices[face.mIndices[0]].z;
				p2.x = mesh->mVertices[face.mIndices[1]].x;
				p2.y = mesh->mVertices[face.mIndices[1]].y;
				p2.z = mesh->mVertices[face.mIndices[1]].z;
				p3.x = mesh->mVertices[face.mIndices[2]].x;
				p3.y = mesh->mVertices[face.mIndices[2]].y;
				p3.z = mesh->mVertices[face.mIndices[2]].z;
				glm:vec3 edge1 = p2 - p1;
				glm::vec3 edge2 = p3 - p1;
				glm::vec3 normal = glm::cross(edge1, edge2)/glm::length(edge1 - edge2);
				in_tbnList[i].normal = normal;
				in_tbnList[i + 1].normal = normal;
				in_tbnList[i + 2].normal = normal;
			}
		}
	}
	if (!hasTangent)
	{
		computeTangentBasis(in_vertices, faces, in_tbnList);
	}
	// material 처리
	if (mesh->mMaterialIndex >= 0)
	{
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		vector<Texture> diffuseMaps = loadMaterialTextures(material,
			aiTextureType_DIFFUSE, Texture::TextureType::Diffuse);
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		vector<Texture> specularMaps = loadMaterialTextures(material,
			aiTextureType_SPECULAR, Texture::TextureType::Specular);
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		vector<Texture> normalMaps = loadMaterialTextures(material,
			aiTextureType_NORMALS, Texture::TextureType::Normal);
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		vector<Texture> heightMaps = loadMaterialTextures(material,
			aiTextureType_HEIGHT, Texture::TextureType::Normal);
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}
	
	indexVBO_TBN(in_vertices, in_tbnList, out_indices, out_vertices, out_tbnList);
	return new CMesh(in_vertices, in_tbnList, in_indices, textures);
	return new CMesh(out_vertices, out_tbnList, out_indices, textures);
}
vector<Texture> CModel::loadMaterialTextures(aiMaterial *mat, aiTextureType type, Texture::TextureType typeName)
{
	vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;

		mat->GetTexture(type, i, &str);
		bool skip = false;
		Texture* tmpTex = CResources::FindLoadedTextureWithPath(str.C_Str());
		if (tmpTex != nullptr)
		{
			textures.push_back(*tmpTex);
			skip = true;
			break;
		}
		if (!skip)
		{   // 텍스처가 이미 불러와져있지 않다면 불러옵니다.
			Texture* texture = new Texture();
			texture->id = CResources::TextureFromFile(str.C_Str(), directory);
			texture->textureType = typeName;
			texture->path = str.C_Str();
			textures.push_back(*texture);
			CResources::loadedTexturesMap[texture->id] = texture;
		}
	}
	return textures;
}