#include "Components.h"
#include "GameObject.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace glm;
CCamera* CCamera::mainCam;
float CCamera::GetViewRatio()
{
	return this->viewRatio.x / this->viewRatio.y;
}
void CCamera::UpdateViewMatrix()
{
	
	glm::vec3 direction = gameObject->transform->forward;
	mViewMatrix = glm::lookAt(
		gameObject->transform->GetWorldPosition(),
		gameObject->transform->GetWorldPosition() + direction,
		gameObject->transform->up
	);
	//mViewMatrix = gameObject->transform->GetWorldMatrix();
}
void CCamera::UpdatePerspectiveCamera()
{
	mProjMatrix = glm::perspective(
		glm::radians(fov),
		GetViewRatio(),
		near,
		far
	);
}
void CCamera::UpdateOrthographicCamera()
{
	mProjMatrix = glm::ortho(
		0.0f, 
		1920.0f, 
		1080.0f, 
		0.0f,
		0.1f, 
		1000.0f);
}
void CCamera::UpdateProjectionMatrix()
{
	(this->*camUpdateMethod[(int)camType])();
}
void CCamera::UpdateMVP()
{
	MVP = mProjMatrix * mViewMatrix * glm::mat4(1.0f);
}
glm::mat4 CCamera::GetProjectionMatrix()
{
	return mProjMatrix;
}
glm::mat4 CCamera::GetViewMatrix()
{
	return mViewMatrix;
}
glm::mat4 CCamera::GetMVP()
{
	return MVP;
}
void CCamera::Start()
{

}
void CCamera::Update()
{
	UpdateViewMatrix();
	UpdateProjectionMatrix();
	UpdateMVP();
}