#include "UniObject.h"
#include "TypeHelper.h"
#include "CTime.h"
#include "CInput.h"
#include "Scene.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include <map>
#pragma once

class CTransform : public UniComponent
{
public:
	//	Constructor Block
	CTransform()
	{
		name = "Transform";
		parent = NULL;
		childCount = 0;
		children = std::vector<CTransform*>();
		SetLocalScale(glm::vec3(1, 1, 1));
	}
private:
	glm::mat4 mWorldMatrix;
	glm::mat4 mLocalMatrix;
	CTransform* parent;
	glm::vec3 mWorldPosition;
	glm::vec3 mLocalPosition;
	glm::vec3 mWorldScale;
	glm::vec3 mLocalScale;
	glm::vec3 mWorldRotation;
	glm::vec3 mLocalRotation;
public:
	glm::vec3 right, up, forward;
	std::vector<CTransform*> children;
	int childCount;

	void SetParent(CTransform* parent);
	CTransform* GetParent();

	void SetWorldPosition(glm::vec3 pos);
	void SetLocalPosition(glm::vec3 pos);
	void SetWorldRotation(glm::vec3 rotation);
	void SetLocalRotation(glm::vec3 rotation);
	void SetWorldScale(glm::vec3 scale);
	void SetLocalScale(glm::vec3 scale);
	glm::vec3 GetWorldPosition();
	glm::vec3 GetLocalPosition();
	glm::quat GetWorldRotation();
	glm::quat GetLocalRotation();
	glm::vec3 GetWorldRotationVec3();
	glm::vec3 GetLocalRotationVec3();
	glm::vec3 GetWorldScale();
	glm::vec3 GetLocalScale();

	void Translate(glm::vec3 delta);
	void Rotate(glm::vec3 delta);
	void Scale(glm::vec3 delta);

	void UpdateWorldMatrix();
	glm::mat4 GetWorldMatrix();

	// UniObject을(를) 통해 상속됨
	void Start() override;
	void Update() override;
};

class CRigid : public UniComponent
{
public:
	float mass;
	float drag;
	float dragAngle;
	glm::vec3 velocity;
	glm::vec3 angleVelocity;
	bool useGravity;
	glm::vec3 gravity;

	CRigid()
	{
		mass = 1;
		drag = 0;
		dragAngle = 0;
		useGravity = true;
		gravity.y = 9.8f;
	}
	// UniObject을(를) 통해 상속됨
	void Start() override;
	void Update() override;
	void PhysicsUpdate();

	void NoneGravityUpdate();
	void GravityUpdate();
	void AddForce(glm::vec3 force);
};

class CCamera : public UniComponent
{
public:
	CCamera()
	{
		name = "Camera";
		fov = 60;
		viewRatio = glm::vec2(16, 9);
		near = 0.1;
		far = 1000;
		camType = (CamaraType)0;
	}
private:
	glm::mat4 mViewMatrix;
	glm::mat4 mProjMatrix;
	glm::mat4 MVP;
public:
	static CCamera* mainCam;
	enum CamaraType {Perspective = 0, Orthographic};
	CamaraType camType;
	float fov;
	glm::vec2 viewRatio;
	float near, far;
	float GetViewRatio();
	
	void UpdateViewMatrix();
	void UpdateProjectionMatrix();
	void UpdateMVP();
	glm::mat4 GetViewMatrix();
	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetMVP();

	//	아래 두 함수는 UpdateProjecionMatrix에서 호출된다.
	void UpdatePerspectiveCamera();
	void UpdateOrthographicCamera();
	void (CCamera::*camUpdateMethod[2])() = { &CCamera::UpdatePerspectiveCamera,&CCamera::UpdateOrthographicCamera };

	// UniObject을(를) 통해 상속됨
	void Start() override;
	void Update() override;
};
class CLight : public UniComponent
{
public:
	enum LightType {DIRECTIONAL = 0, POINT};
	LightType lightType;

	float intensity = 1;
	float range = 10;

	glm::vec3 ambient = glm::vec3(0.1,0.1,0.1);
	glm::vec3 diffuse = glm::vec3(0.8,0.8,0.8);
	glm::vec3 specular = glm::vec3(0.5,0.5,0.5);

	float constant = 0.2;
	float linear = 0.03;
	float quadratic = 0.016;

	void SetDirectionalLight(
		glm::vec3 ambient = glm::vec3(0.1,0.1,0.1),
		glm::vec3 diff = glm::vec3(0.8,0.8,0.8), 
		glm::vec3 spec = glm::vec3(0.5,0.5,0.5))
	{
		this->lightType = LightType::DIRECTIONAL;
		this->ambient = ambient;
		this->diffuse = diff;
		this->specular = spec;
	}
	void SetPointLight(
		glm::vec3 ambient = glm::vec3(0.1, 0.1, 0.1),
		glm::vec3 diff = glm::vec3(0.8, 0.8, 0.8),
		glm::vec3 spec = glm::vec3(0.5, 0.5, 0.5),
		float constant = 0.2,
		float linear = 0.03,
		float quadratic = 0.016
	)
	{
		this->lightType = LightType::POINT;
		this->ambient = ambient;
		this->diffuse = diff;
		this->specular = spec;

		this->constant = constant;
		this->linear = linear;
		this->quadratic = quadratic;
	}

	// UniObject을(를) 통해 상속됨
	void Start() override;
	void Update() override;
};
class CShader
{
public:
	unsigned int id;

	CShader(const GLchar* vertPath, const GLchar* fragPath);

	void Use();

	// Uniform Value Utility Func
	void SetBool(const std::string& name, bool value) const;
	void SetInt(const std::string& name, int value) const;
	void SetFloat(const std::string& name, float value) const;
	void SetVec2(const std::string &name, const glm::vec2 &value) const;
	void SetVec2(const std::string &name, float x, float y) const;
	// ------------------------------------------------------------------------
	void SetVec3(const std::string &name, const glm::vec3 &value) const;
	void SetVec3(const std::string &name, float x, float y, float z) const;
	// ------------------------------------------------------------------------
	void SetVec4(const std::string &name, const glm::vec4 &value) const;
	void SetVec4(const std::string &name, float x, float y, float z, float w);
	// ------------------------------------------------------------------------
	void SetMat2(const std::string &name, const glm::mat2 &mat) const;
	// ------------------------------------------------------------------------
	void SetMat3(const std::string &name, const glm::mat3 &mat) const;
	// ------------------------------------------------------------------------
	void SetMat4(const std::string &name, const glm::mat4 &mat) const;
};
struct Vertex
{
	glm::vec3 position;
	glm::vec2 texcoord;
};
struct TBN
{
	glm::vec3 normal;
	glm::vec3 tangent;
	glm::vec3 biTangent;
};
struct Texture
{
	int id;
	std::string path;
	enum TextureType
	{
		Diffuse,
		Normal,
		Specular,
		Height
	};
	TextureType textureType;
};
class CMesh
{
public:
	//	Vertex and Indices for 3D Model
	std::vector<Vertex>			vertices;
	std::vector<TBN>			tbnList;
	std::vector<unsigned short> indices;
	std::vector<Texture> textures;

	CMesh(
		std::vector<Vertex> vertices,
		std::vector<TBN>	tbnList,
		std::vector<unsigned short> indices,
		std::vector<Texture> textures
	);
	void Draw(CShader shader);
private:
	unsigned int VAO, VBO, EBO, VBO_TBN;
	void SetUpMesh();
};
class CModel
{
public:
	std::vector<CMesh*> meshes;
	std::string directory;
	GameObject* LoadModel(std::string path);
private:
	void processNode(aiNode *node, GameObject* targetParent, const aiScene *scene);
	CMesh* processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, Texture::TextureType typeName);
};
class Material
{
public:
	GLuint vboID;			//	it saved in MESH
	CShader* shader;		//  NEED
	GLuint vertexBuffer;
	GLuint uvBuffer;
	GLuint normalBuffer;
	GLuint tangentBuffer;
	GLuint elementBuffer;
	std::map <std::string, GLuint> idMap;
	std::vector<GLuint> textureVector;
	std::vector<GLuint> textureIDVector;
	bool useCubemap = false;
	GLuint cubeMap;
	GLuint cubeMapID;
	bool useTiling = false;
	glm::vec2 tiling = glm::vec2(1, 1);

	Material(CShader* shader)
	{
		this->shader = shader;
	}

	GLuint GetTextureID(const GLchar* sampleName)
	{
		return glGetUniformLocation(shader->id, sampleName);
	}
	GLuint GetID(const char* propName)
	{
		return glGetUniformLocation(shader->id, propName);
	}
};
class CRenderer : public UniComponent
{
public:	
	CRenderer()
	{
		name = "Model Renderer";
	}
	//CModel* model;

	CMesh* mesh;
	Material* material;

	// UniObject을(를) 통해 상속됨
	void Start() override;
	void Update() override;
};