#include<assimp/Importer.hpp>
#include<assimp/scene.h>
#include<assimp/postprocess.h>

#include "Components.h"
#include "GameObject.h"
#include "Skybox.h"
#include <common/shader.hpp>
#include <common/texture.hpp>
#include "common/tangentspace.hpp"
#include "common/vboindexer.hpp"


// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#pragma once
using namespace std;
using namespace glm;

void CRenderer::Start()
{
	//	material->shaderID는 Application의 BuildDeviceResource에서 정의해줌
	//	material->texture와 textureID
	/*
	glGenVertexArrays(1, &material->vboID);
	glBindVertexArray(material->vboID);
	
	glGenBuffers(1, &material->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, material->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, mesh.indexed_vertices.size() * sizeof(glm::vec3), &mesh.indexed_vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &material->uvBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, material->uvBuffer);
	if (mesh.indexed_uvs.size() != 0)
		glBufferData(GL_ARRAY_BUFFER, mesh.indexed_uvs.size() * sizeof(glm::vec2), &mesh.indexed_uvs[0], GL_STATIC_DRAW);

	glGenBuffers(1, &material->normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, material->normalBuffer);
	if (mesh.indexed_normals.size() != 0)
		glBufferData(GL_ARRAY_BUFFER, mesh.indexed_normals.size() * sizeof(glm::vec3), &mesh.indexed_normals[0], GL_STATIC_DRAW);
	
	glGenBuffers(1, &material->tangentBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, material->tangentBuffer);
	if (mesh.indexed_tangents.size() != 0)
		glBufferData(GL_ARRAY_BUFFER, mesh.indexed_tangents.size() * sizeof(glm::vec3), &mesh.indexed_tangents[0], GL_STATIC_DRAW);

	glGenBuffers(1, &material->elementBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, material->elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.indices.size() * sizeof(unsigned short), &mesh.indices[0], GL_STATIC_DRAW);
	//	Define Default Property ID
	material->SetID("model");
	material->SetID("view");
	material->SetID("proj");
	material->SetID("lightPos0");	//	Directional Light
	material->SetID("viewPos");		//	Main Camera Position
	material->SetID("tiling");
	*/
}

void CRenderer::Update()
{
	material->shader->Use();
	
	//	MVP Setting
	glm::mat4 m = gameObject->transform->GetWorldMatrix();
	glm::mat4 v = CCamera::mainCam->GetViewMatrix();
	glm::mat4 p = CCamera::mainCam->GetProjectionMatrix();
	
	material->shader->SetMat4("model", m);
	material->shader->SetMat4("view", v);
	material->shader->SetMat4("proj", p);
	
	//	Lighting Uniform Value 채우기
	glm::vec3 lightPos = glm::vec3(2, 20, 5);
	glm::vec3 camPos = CCamera::mainCam->gameObject->transform->GetWorldPosition();
	material->shader->SetVec3("dirLight.direction", -0.5f,0.0f,0.0f);
	material->shader->SetVec3("dirLight.ambient", 0.1f, 0.1f, 0.1f);
	material->shader->SetVec3("dirLight.diffuse", 0.1f, 0.1f, 0.1f);
	material->shader->SetVec3("dirLight.specular", 0.2f, 0.2f, 0.2f);

	material->shader->SetFloat("pointLight[0].intensity", 5);
	material->shader->SetFloat("pointLight[0].range", 200);
	material->shader->SetVec3("pointLight[0].position", lightPos);
	material->shader->SetVec3("pointLight[0].ambient", glm::vec3(0.1, 0.1, 0.1));
	material->shader->SetVec3("pointLight[0].diffuse", 1, 1, 1);
	material->shader->SetVec3("pointLight[0].specular", 1, 1, 1);
	material->shader->SetFloat("pointLight[0].constant", 0.2);
	material->shader->SetFloat("pointLight[0].linear", 0.03);
	material->shader->SetFloat("pointLight[0].quadratic", 0.016);

	material->shader->SetVec3("lightPos[0]", lightPos);
	material->shader->SetVec3("viewPos", camPos);
	
	//	Tiling
	if (material->useTiling)
	{
		glUniform2f(material->GetID("tiling"), material->tiling.x, material->tiling.y);
	}
	else
	{
		glUniform2f(material->GetID("tiling"), 1, 1);
	}
	mesh->Draw(*material->shader);
	//model->Draw(*material->shader);
	//glUseProgram(0);
	/*
	glBindVertexArray(material->vboID);
	//	Texturing
	for (int i = 0; i < material->textureVector.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, material->textureVector[i]);
		glUniform1i(material->textureIDVector[i], i);
	}
	*/
	//if (material->useCubemap)
	//{
	//	glActiveTexture(GL_TEXTURE0 + 3);
	//	glBindTexture(GL_TEXTURE_CUBE_MAP, material->cubeMap);
	//	glUniform1i(material->cubeMapID, 3);
	//}
	//	Location 0에 Vertex Binding
	/*
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, material->vertexBuffer);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);
	//	Location 1에 UV Binding
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, material->uvBuffer);
	glVertexAttribPointer(
		1,                                // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	//	Location 2에 Normal Binding
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, material->normalBuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	//	Location 3에 Tangent Binding
	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, material->tangentBuffer);
	glVertexAttribPointer(
		3,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	//	Vertex Index Binding
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, material->elementBuffer);
	//	Drawing
	glDrawElements(
		GL_TRIANGLES,      // mode
		mesh.indices.size(),    // count
		GL_UNSIGNED_SHORT,   // type
		(void*)0           // element array buffer offset
	);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glBindVertexArray(0);
	glUseProgram(0);
	*/
}
