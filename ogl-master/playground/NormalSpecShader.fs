#version 330 core
out vec4 FragColor;

#define NR_POINT_LIGHTS 4
in VS_OUT{
	vec3 FragPos;
    vec2 TexCoords;
	mat3 TBN;
    vec3 TangentLightPos[NR_POINT_LIGHTS];
    vec3 TangentViewPos;
    vec3 TangentFragPos;
}fs_in;

struct DirLight{
	float intensity;
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
struct PointLight{
	float intensity;
	float range;
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;
};

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_specular1;

uniform DirLight dirLight;
uniform PointLight pointLight[NR_POINT_LIGHTS];
uniform vec2 tiling;
uniform vec3 lightPos;
uniform vec3 viewPos;

vec3 CalcDirLight(vec3 viewDir, vec3 normal, DirLight light);
vec3 CalcPointLight(vec3 lightDir, vec3 viewDir, vec3 normal, PointLight light);

void main()
{
	vec3 normal = texture(texture_normal1, fs_in.TexCoords * tiling).rgb;
	normal = normalize(normal);
	//	In Tangent Space
	normal = normalize(normal * 2.0 - 1.0);
	normal = normalize(fs_in.TBN * normal);

	vec3 tan = fs_in.TBN[0];
	vec3 biTan = fs_in.TBN[1];
	vec3 calcN = cross(tan, biTan);
	float normalAligned = dot(calcN, fs_in.TBN[2]);
	if (normalAligned < 0)
	{
		normal = -normal;
	}
	vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
	//vec3 result = vec3(0,0,0);
	vec3 result = CalcDirLight(viewDir, normal, dirLight);
	for (int i = 0; i < NR_POINT_LIGHTS; i++)
	{
		vec3 lightDir = normalize(fs_in.TangentLightPos[i] - fs_in.TangentFragPos);
		result += CalcPointLight (lightDir, viewDir, normal, pointLight[i]);
	}
	FragColor = vec4(result, 1.0);
}
vec3 CalcPointLight(vec3 lightDir, vec3 viewDir, vec3 normal, PointLight light)
{
	float diff = max(dot(lightDir, normal), 0.0);
	vec3 color = texture(texture_diffuse1, fs_in.TexCoords).rgb;

	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir +  viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
		
	float dis = length(light.position - fs_in.FragPos);
	float attenuation = 1 / (light.constant + light.linear * dis + light.quadratic * dis * dis);
	//attenuation *= max(0,1 - dis/light.range);

	vec3 ambient = light.ambient * color;
	vec3 diffuse = light.diffuse * diff * color;
	vec3 specular = spec * texture(texture_specular1, fs_in.TexCoords).rgb;
	return (ambient + diffuse + specular) * attenuation;
}
vec3 CalcDirLight(vec3 viewDir, vec3 normal, DirLight light)
{
	vec3 lightDir = normalize(fs_in.TBN * light.direction);
	float diff = max(dot(lightDir, normal), 0.0);
	vec3 color = texture(texture_diffuse1, fs_in.TexCoords).rgb;

	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
		

	vec3 ambient = light.ambient * color;
	vec3 diffuse = light.diffuse * diff * color;
	vec3 specular = spec * texture(texture_specular1, fs_in.TexCoords).rgb;
	return (ambient + diffuse + specular);
}