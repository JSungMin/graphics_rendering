#include <string>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#pragma once

class GameObject;
class Scene
{
public:
	std::string name;
	//	SceneManager에 따로 뺄 것
	static Scene* currentScene;
	static GLFWwindow* window;
private:
	//	최상위 GameObject만 담기며
	//	자식들은 GameObject의 transform 아래 children을 통해 탐지
	std::vector<GameObject*> hierarchy;
public:
	//	부모 없는 Root 객체들만 넣을 수 있으며
	//	부모가 있는 경우엔 Tree Traversal로 접근할 것
	void PushToHierarchy(GameObject* obj);
	void PopFromHierarchy(GameObject* obj);
	//	DFS Traversal로 GameObject내 모든 Component의 Start 호출
	void StartScene();
	//	DFS Traversal로 GameObject내 모든 Component의 Update 호출
	void UpdateScene();
	void UpdatePhysics();
};
