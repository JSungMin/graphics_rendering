#include "CPrimitiveMesh.h"

CRenderer::CMesh CPrimitiveMesh::GetCube(glm::vec3 extent)
{
	CRenderer::CMesh cube;
	cube.indexed_uvs = std::vector<glm::vec2>();
	cube.indexed_normals = std::vector<glm::vec3>();
	cube.indexed_vertices = {
		glm::vec3(1,  1,  1), glm::vec3(1, 1, 1), // 0
		glm::vec3(-1,  1,  1), glm::vec3(0, 1, 1), // 1
		glm::vec3(-1, -1,  1), glm::vec3(0, 0, 1), // 2
		glm::vec3(1, -1,  1), glm::vec3(1, 0, 1), // 3
		glm::vec3(1, -1, -1), glm::vec3(1, 0, 0), // 4
		glm::vec3(-1, -1, -1), glm::vec3(0, 0, 0), // 5
		glm::vec3(-1,  1, -1), glm::vec3(0, 1, 0), // 6
		glm::vec3(1,  1, -1), glm::vec3(1, 1, 0) // 7
	};
	
	//	Drawing Foward
	cube.indices = {
		0, 1, 2, 3,                 // Front face
		7, 4, 5, 6,                 // Back face
		6, 5, 2, 1,                 // Left face
		7, 0, 3, 4,                 // Right face
		7, 6, 1, 0,                 // Top face
		3, 2, 5, 4                 // Bottom face
	};
	return cube;
}

CRenderer::CMesh CPrimitiveMesh::GetSphere(glm::vec3 extent)
{
	return CRenderer::CMesh();
}
