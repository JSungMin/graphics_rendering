#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
GLFWwindow* window;
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <common/shader.hpp>
#include <common/texture.hpp>
#include "core.h"
#include "RocketLauncher.h"
#include "CameraController.h"
#include "Skybox.h"

#pragma once

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

using namespace glm;

int InitializeWindow();
int InitializeGLEW();
bool BuildDeviceResource();
void BuildSceneResource();

/*------------Device Resource Field--------------*/
//	Global Map은 다른 Material에서 Resource 재활용을 할 때를 위해 존재한다.
std::map<std::string, GLuint>					textureMap;
std::map<std::string, CModel*>					modelMap;
/*---------End of Device Resource Field----------*/

bool BuildDeviceResource()
{
	//	Shader 선언 부
	CShader* simpleShader = new CShader("NormalSpecShader.vs", "NormalSpecShader.fs");
	CShader* reflectShader = new CShader("NormalSpecShader.vs", "NormalSpecShader.fs");
	CShader* skyboxShader = new CShader("Skybox.vs", "Skybox.fs");
	
	CResources::shaderMap["SimpleShader"] = simpleShader;
	CResources::shaderMap["SkyboxShader"] = skyboxShader;
	CResources::shaderMap["ReflectShader"] = reflectShader;
	

	//	Texture Load 부
	GLuint defaultTex = loadDDS("Texture/Default.DDS");
	GLuint rBodyTex = loadDDS("Texture/body.DDS");
	GLuint rBodyNormTex = loadDDS("Texture/body_norm.DDS");
	GLuint rBodySpecTex = loadDDS("Texture/body_spec.DDS");
	
	textureMap["Default"] = defaultTex;

#pragma region SkyboxSetting
	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------

	std::vector<const char*> faces
	{
		"Texture/Skybox/right.DDS",
		"Texture/Skybox/left.DDS",
		"Texture/Skybox/top.DDS",
		"Texture/Skybox/bottom.DDS",
		"Texture/Skybox/front.DDS",
		"Texture/Skybox/back.DDS"
	};
	Skybox::Initialize(faces, *CResources::shaderMap["SkyboxShader"]);
#pragma endregion

	//	Material 선언 부
	Material* whiteSimpleMat = new Material(simpleShader);
	whiteSimpleMat->useTiling = false;
	CResources::materialMap["DefaultMat"] = whiteSimpleMat;

	return true;
}
unsigned int quadVAO = 0;
unsigned int quadVBO;
/*-----------Scene Resource Field------------*/
Scene mainScene;
GameObject* playerObj;

GameObject* camObj;
CCamera*	camComp;

GameObject* dirLightObj;
GameObject* pointLightObj;
CLight*		dirLight;
CLight*		pointLight;

/*---------End of Scene Resource Field----------*/
void BuildSceneResource()
{
	Scene::currentScene = &mainScene;
	Scene::window = window;

	//	Model 선언 부
	playerObj = CResources::LoadModelToScene("Obj/nanosuit/nanosuit.obj");
	playerObj->transform->SetWorldRotation(glm::vec3(0, 90, 0));

	camObj = GameObject::Instantiate("Main Camera");
	camObj->AddComponent<CCamera>();
	camObj->AddComponent<CameraController>();
	camComp = camObj->GetComponent<CCamera>();
	camObj->transform->SetWorldPosition(glm::vec3(0, 30, 100));
	camObj->transform->SetWorldRotation(glm::vec3(-20, 180, 0));
	CCamera::mainCam = camComp;

	dirLightObj = GameObject::Instantiate("Directional Light");
	dirLight = dirLightObj->AddComponent<CLight>();
	pointLightObj = GameObject::Instantiate("Point Light0");
	pointLight = pointLightObj->AddComponent<CLight>();
}

int main( void )
{
	// Initialise GLFW
	if (!InitializeWindow())
		return -1;
	// Initialize GLEW
	if (!InitializeGLEW())
		return -1;
	if(!BuildDeviceResource())
		return -1;
	BuildSceneResource();

	
	CTime::curTime = glfwGetTime();
	CInput::window = window;
	Scene::currentScene->StartScene();

	do{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Skybox::Render();
		//	Update Delta Time
		CTime::UpdateDeltaTime();
		//	Update Physics Components
		Scene::currentScene->UpdatePhysics();
		//	Update All Components In Scene
		Scene::currentScene->UpdateScene();
		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );
	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

int InitializeWindow()
{
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1920 * 0.8, 1080 * 0.8, "Playground", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	return 1;
}
int InitializeGLEW()
{
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	
	// Set the mouse at the center of the screen
	glfwPollEvents();
	//glfwSetCursorPos(window, 1024 / 2, 768 / 2);

	// Dark blue background
	glClearColor(0.5f, 0.5f, 0.8f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	//glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	return 1;
}